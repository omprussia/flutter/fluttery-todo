## Updated: 09/16/2024 11:47:11 AM

## Info

- Last tag: 1.1.3
- Released: 2

## Versions

- Version: 1.1.3 (13/09/2024)
- Version: 1.1.2 (24/04/2024)

### Version: 1.1.3 (13/09/2024)

#### Change

- Update dependencies
- Update old textTheme names

#### Feature

- Add information about build_runner

### Version: 1.1.2 (24/04/2024)

#### Change

- Up version Flutter for Aurora OS.
