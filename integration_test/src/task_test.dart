import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../utils/utils.dart';
import 'category_test.dart';
import 'main_tests.dart';

class CreateTaskTest extends CreateCategoryTest {
  Future<void> createTask(
    WidgetTester tester, [
    String taskName = "CrTaskTest",
    String categoryName = "CreateTaskCategory",
  ]) async {
    // Open the main screen
    await MainTests().openMainScreen(tester);

    // Create a category for the test
    await createCategory(tester, categoryName);

    // Scroll to the previously created category
    final category = find.text(categoryName);
    await tester.scrollUntilVisible(category, 10000);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
    await tester.tap(category);

    // Check that the category screen opened
    await tester.pumpAndSettle();
    await pumpUntilFound(tester, category);
    expect(category, findsOneWidget);

    // Click on the add button
    final addTaskButton = find.byKey(Key("fab_new_task"));
    await pumpUntilFound(tester, addTaskButton);
    expect(addTaskButton.hitTestable(), findsOneWidget);
    await tester.tap(addTaskButton);

    await tester.pump(Duration(seconds: 3));

    // Enter the name of the new task
    final taskTextField = find.byKey(Key("task_name_field"));
    await pumpUntilFound(tester, taskTextField);
    await tester.enterText(taskTextField, taskName);

    // Find the save task button
    final createNewButton = find.byKey(Key("fab_new_task"));
    await pumpUntilFound(tester, createNewButton);
    expect(createNewButton.hitTestable(), findsOneWidget);
    await tester.tap(createNewButton);

    // Check that the category screen opened
    await tester.pumpAndSettle();
    await pumpUntilFound(tester, category);
    expect(category, findsOneWidget);

    // Check for the created task's presence
    final taskText = find.byKey(Key(taskName));
    await pumpUntilFound(tester, taskText);
    expect(taskText, findsOneWidget);

    await tester.pumpAndSettle();
    await tester.pageBack();
    await tester.pumpAndSettle();
  }

  Future<void> createTaskWithoutName(
    WidgetTester tester, [
    String categoryName = "CreateTaskWNC",
  ]) async {
    // Open the main screen
    await MainTests().openMainScreen(tester);

    // Create a category for the test
    await createCategory(tester, categoryName);

    // Scroll to the previously created category
    final category = find.text(categoryName);
    await tester.scrollUntilVisible(category, 10000);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
    await tester.tap(category);

    // Check that the category screen opened
    await tester.pumpAndSettle();
    await pumpUntilFound(tester, category);
    expect(category, findsOneWidget);

    // Click on the add button
    final addTaskButton = find.byKey(Key("fab_new_task"));
    await pumpUntilFound(tester, addTaskButton);
    expect(addTaskButton.hitTestable(), findsOneWidget);
    await tester.tap(addTaskButton);

    await tester.pump(Duration(seconds: 3));

    // Find the save task button
    final createNewButton = find.byKey(Key("fab_new_task"));
    await pumpUntilFound(tester, createNewButton);
    expect(createNewButton.hitTestable(), findsOneWidget);
    await tester.tap(createNewButton);

    // Check for the presence of an error message
    final errorMessage = find.text(
        'Ummm... It seems that you are trying to add an invisible task which is not allowed in this realm.');
    await pumpUntilFound(tester, errorMessage);
    expect(errorMessage, findsOneWidget);
  }
}

class CheckBoxTaskTest extends CreateTaskTest {
  Future<void> checkBoxTaskTest(
    WidgetTester tester, [
    String taskName = "cbTaskTest",
    String categoryName = "cbtTestCategory",
  ]) async {
    await createTask(tester, taskName, categoryName);

    // Open the main screen
    await MainTests().openMainScreen(tester);

    // Scroll to the previously created category
    final category = find.text(categoryName);
    await tester.scrollUntilVisible(category, 10000);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
    await tester.tap(category);

    // Check that the category screen opened
    await tester.pumpAndSettle();
    await pumpUntilFound(tester, category);
    expect(category, findsOneWidget);

    // Check for the presence of the created task
    final taskText = find.byKey(Key(taskName));
    await pumpUntilFound(tester, taskText);
    expect(taskText, findsOneWidget);

    // Check the state of the task checkbox
    var taskCheckBoxFiner = find.byKey(Key(taskName + "_checkBox"));
    var taskCheckBox = tester.firstWidget<Checkbox>(taskCheckBoxFiner);
    expect(taskCheckBox.value, false);

    // Click on the task to complete it
    await tester.tap(taskCheckBoxFiner);
    await tester.pumpAndSettle();
    taskCheckBox = tester.firstWidget<Checkbox>(taskCheckBoxFiner);
    expect(taskCheckBox.value, true);

    // Uncheck the task
    await tester.tap(taskCheckBoxFiner);
    await tester.pumpAndSettle();
    taskCheckBox = tester.firstWidget<Checkbox>(taskCheckBoxFiner);
    expect(taskCheckBox.value, false);
  }
}

class ProgressTaskTest extends CreateTaskTest {
  Future<void> progressTaskTest(
    WidgetTester tester, [
    String taskName = "pTaskTest",
    String categoryName = "ptTestCategory",
  ]) async {
    await createTask(tester, taskName, categoryName);

    // Open the main screen
    await MainTests().openMainScreen(tester);

    // Scroll to the previously created category
    final category = find.text(categoryName);
    await tester.scrollUntilVisible(category, 10000);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
    await tester.tap(category);

    // Check that the category screen opened
    await tester.pumpAndSettle();
    await pumpUntilFound(tester, category);
    expect(category, findsOneWidget);

    // Check for the presence of the created task
    final taskText = find.byKey(Key(taskName));
    await pumpUntilFound(tester, taskText);
    expect(taskText, findsOneWidget);

    // Check the state of the task checkbox
    var taskCheckBoxFiner = find.byKey(Key(taskName + "_checkBox"));
    var taskCheckBox = tester.firstWidget<Checkbox>(taskCheckBoxFiner);
    expect(taskCheckBox.value, false);

    // Check that the progress is 0%
    final zeroProgressFinder = find.text("0%");
    await pumpUntilFound(tester, zeroProgressFinder);
    expect(zeroProgressFinder, findsOneWidget);

    // Click on the task to complete it
    await tester.tap(taskCheckBoxFiner);
    await tester.pumpAndSettle();
    taskCheckBox = tester.firstWidget<Checkbox>(taskCheckBoxFiner);
    expect(taskCheckBox.value, true);

    // Check that the progress is 100%
    final completeProgressFinder = find.text("100%");
    await pumpUntilFound(tester, completeProgressFinder);
    expect(completeProgressFinder, findsOneWidget);

    // Uncheck the task
    await tester.tap(taskCheckBoxFiner);
    await tester.pumpAndSettle();
    taskCheckBox = tester.firstWidget<Checkbox>(taskCheckBoxFiner);
    expect(taskCheckBox.value, false);

    // Check that the progress is 0%
    await pumpUntilFound(tester, zeroProgressFinder);
    expect(zeroProgressFinder, findsOneWidget);
  }
}

class DeleteTestTask extends CreateTaskTest {
  Future<void> deleteTask(
    WidgetTester tester, [
    String taskName = "dTaskTest",
    String categoryName = "dTestCategory",
  ]) async {
    await createTask(tester, taskName, categoryName);

    // Opening the main screen
    await MainTests().openMainScreen(tester);

    // Scroll to the category created before
    final category = find.text(categoryName);
    await tester.scrollUntilVisible(category, 10000);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
    await tester.tap(category);

    // Checking that the category screen has opened
    await tester.pumpAndSettle();
    await pumpUntilFound(tester, category);
    expect(category, findsOneWidget);

    // Checking for a created task
    final taskText = find.byKey(Key(taskName));
    await pumpUntilFound(tester, taskText);
    expect(taskText, findsOneWidget);

    // Disposal
    final deleteIcon = find.byKey(Key("btn_remove_todo" + taskName));
    await pumpUntilFound(tester, deleteIcon);
    expect(deleteIcon, findsOneWidget);
    await tester.tap(deleteIcon);
  }
}
