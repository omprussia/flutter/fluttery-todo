import 'package:flutter_test/flutter_test.dart';
import 'package:todo/main.dart' as app;
import 'package:todo/utils/datetime_utils.dart';

import '../utils/utils.dart';

class MainTests {
  /// Checking that the main screen has opened
  Future<void> mainScreenIsOpen(WidgetTester tester) async {
    final fab = find.text(DateTimeUtils.currentDay);
    await pumpUntilFound(tester, fab);

    expect(fab, findsOneWidget);
  }

  /// Open the main screen and check that it is open
  Future<void> openMainScreen(WidgetTester tester) async {
    await tester.pumpWidget(app.MyApp());

    final fab = find.text(DateTimeUtils.currentDay);
    await pumpUntilFound(tester, fab);

    expect(fab, findsOneWidget);
  }
}
