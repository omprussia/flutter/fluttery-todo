import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../utils/utils.dart';
import 'main_tests.dart';

class CreateCategoryTest {
  Future<void> createCategory(WidgetTester tester,
      [String categoryName = "AddCategoryTest"]) async {
    // Opening the main screen
    await MainTests().openMainScreen(tester);

    // Scroll to the add category button
    await tester.scrollUntilVisible(find.text('Add Category'), 10000);
    await tester.pumpAndSettle();
    expect(find.text('Add Category').hitTestable(), findsOneWidget);
    await tester.tap(find.text('Add Category'));

    // Entering the name of a new category
    final categoryText = find.byKey(Key("category_name_field"));
    await pumpUntilFound(tester, categoryText);
    await tester.enterText(categoryText, categoryName);

    // Search for the category save button
    final createNewButton = find.text('Create New Card');
    await pumpUntilFound(tester, createNewButton);
    expect(createNewButton.hitTestable(), findsOneWidget);
    await tester.tap(find.text('Create New Card'));

    // Checking that the main screen has opened
    await tester.pumpAndSettle();
    await MainTests().mainScreenIsOpen(tester);

    // Scroll to the created category and check its availability
    final category = find.text(categoryName);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
  }

  Future<void> createGroupWithoutName(WidgetTester tester) async {
    // Opening the main screen
    await MainTests().openMainScreen(tester);

    // Scroll to the add category button
    await tester.scrollUntilVisible(find.text('Add Category'), 10000);
    expect(find.text('Add Category').hitTestable(), findsOneWidget);
    await tester.tap(find.text('Add Category'));

    // Search for the category save button
    final createNewButton = find.text('Create New Card');
    await pumpUntilFound(tester, createNewButton);
    expect(createNewButton.hitTestable(), findsOneWidget);
    await tester.tap(find.text('Create New Card'));

    // Checking for an error message
    final errorMessage = find.text(
        'Ummm... It seems that you are trying to add an invisible task which is not allowed in this realm.');
    await pumpUntilFound(tester, errorMessage);
    expect(errorMessage, findsOneWidget);
  }
}

class EditCategoryTest extends CreateCategoryTest {
  Future<void> editCategory(WidgetTester tester,
      [String categoryName = "EditCategoryTest"]) async {
    await createCategory(tester, categoryName);

    // Opening the main screen
    await MainTests().openMainScreen(tester);

    // Scroll to the category created before
    final category = find.text(categoryName);
    await tester.scrollUntilVisible(category, 10000);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
    await tester.tap(category);

    // Renaming
    final editIcon = find.byIcon(Icons.edit);
    await pumpUntilFound(tester, editIcon);
    expect(editIcon, findsOneWidget);
    await tester.tap(editIcon);

    // Entering the name of a new category
    final categoryText = find.byKey(Key("category_name_field"));
    await pumpUntilFound(tester, categoryText);
    await tester.enterText(categoryText, categoryName + "_EDITED");

    // Search for the category save button
    final saveChangesButton = find.text('Save Changes');
    await pumpUntilFound(tester, saveChangesButton);
    expect(saveChangesButton.hitTestable(), findsOneWidget);
    await tester.tap(find.text('Save Changes'));

    // Checking for saving changes
    await tester.pumpAndSettle();
    final categoryNew = find.text(categoryName + "_EDITED");
    await pumpUntilFound(tester, categoryNew);
    expect(categoryNew, findsOneWidget);
  }
}

class DeleteCategoryTest extends CreateCategoryTest {
  Future<void> deleteCategory(WidgetTester tester) async {
    final categoryName = "Del_Category";
    await createCategory(tester, categoryName);

    // Opening the main screen
    await MainTests().openMainScreen(tester);

    // Scroll to the category created before
    final category = find.text(categoryName);
    await tester.scrollUntilVisible(category, 10000);
    await pumpUntilFound(tester, category);
    expect(category.hitTestable(), findsOneWidget);
    await tester.tap(category);

    // Disposal
    final deleteIcon = find.byIcon(Icons.delete);
    await pumpUntilFound(tester, deleteIcon);
    expect(deleteIcon, findsOneWidget);
    await tester.tap(deleteIcon);
    final deleteButton = find.text("Delete");
    await pumpUntilFound(tester, deleteButton);
    expect(deleteButton, findsOneWidget);
    await tester.tap(deleteButton);
  }
}
