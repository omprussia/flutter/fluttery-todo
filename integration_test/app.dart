import 'package:integration_test/integration_test.dart';
import 'package:todo/main.dart' as app;
import 'test/main_screen_test.dart' as main_screen_test;
import 'test/category_test.dart' as category_test;
import 'test/task_test.dart' as task_test;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  app.main();

  main_screen_test.main();
  category_test.main();
  task_test.main();
}
