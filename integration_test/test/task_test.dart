import 'package:flutter_test/flutter_test.dart';

import '../src/task_test.dart';

void main() {
  group('Task-Test', () {
    testWidgets('Add-Task-Test-Without-Name', (tester) async {
      await CreateTaskTest().createTaskWithoutName(tester);
    });

    testWidgets('Add-Task-Test', (tester) async {
      await CreateTaskTest().createTask(tester);
    });

    testWidgets('CheckBox-Task-Test', (tester) async {
      await CheckBoxTaskTest().checkBoxTaskTest(tester);
    });

    testWidgets('Progress-Task-Test', (tester) async {
      await ProgressTaskTest().progressTaskTest(tester);
    });

    testWidgets('Delete-Task-Test', (tester) async {
      await DeleteTestTask().deleteTask(tester);
    });
  });
}
