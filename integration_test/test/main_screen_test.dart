import 'package:flutter_test/flutter_test.dart';

import '../src/main_tests.dart';

void main() {
  group('Main-Screen-Test', () {
    testWidgets('Day-Test', (tester) async {
      await MainTests().openMainScreen(tester);
    });
  });
}
