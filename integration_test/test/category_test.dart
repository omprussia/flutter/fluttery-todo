import 'package:flutter_test/flutter_test.dart';

import '../src/category_test.dart';

void main() {
  group('Category-Test', () {
    testWidgets('Add-Category-Without-Name', (tester) async {
      await CreateCategoryTest().createGroupWithoutName(tester);
    });

    testWidgets('Add-Category', (tester) async {
      await CreateCategoryTest().createCategory(tester);
    });

    testWidgets('Edit-Category', (tester) async {
      await EditCategoryTest().editCategory(tester);
    });

    testWidgets('Delete-Category', (tester) async {
      await DeleteCategoryTest().deleteCategory(tester);
    });
  });
}
