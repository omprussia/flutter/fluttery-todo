import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// Widget waiting utility
Future<void> pumpUntilFound(
  WidgetTester tester,
  Finder finder, {
  Duration timeout = const Duration(seconds: 10),
}) async {
  bool timerDone = false;
  final timer = Timer(timeout, () => timerDone = true);
  while (timerDone != true) {
    await tester.pump();

    final found = tester.any(finder);
    if (found) {
      timerDone = true;
    }
  }
  timer.cancel();
}

// Utility for performing a tap on an element
Future<void> tapUntilFound(
  WidgetTester tester,
  Finder finder, {
  Duration timeout = const Duration(seconds: 10),
}) async {
  await pumpUntilFound(tester, finder, timeout: timeout);
  await tester.tap(finder);
}

// Utility for checking the presence of text on the screen
Future<void> expectTextFound(
  WidgetTester tester,
  String expectedText, {
  Duration timeout = const Duration(seconds: 10),
}) async {
  final finder = find.text(expectedText);
  await pumpUntilFound(tester, finder, timeout: timeout);
}

// Widget waiting utility (Analogous to pumpUntilFound)
Future<void> waitForWidgetToAppear(
  WidgetTester tester,
  Finder finder, {
  Duration timeout = const Duration(seconds: 10),
}) async {
  final endTime = DateTime.now().add(timeout);
  while (DateTime.now().isBefore(endTime)) {
    await tester.pump();
    if (tester.any(finder)) return;
  }
  throw TimeoutException('Виджет не найден: $finder за $timeout');
}

// Utility for pressing a button and waiting for the result
Future<void> tapAndWaitForCondition(
  WidgetTester tester,
  Finder buttonFinder,
  Finder expectedFinder, {
  Duration timeout = const Duration(seconds: 10),
}) async {
  await tester.tap(buttonFinder);
  await tester.pumpAndSettle();
  await waitForWidgetToAppear(tester, expectedFinder, timeout: timeout);
}

// Utility for checking the input field
Future<void> expectTextFieldValue(
  WidgetTester tester,
  Finder textFieldFinder,
  String expectedValue,
) async {
  await tester.pumpAndSettle();
  final textField = tester.widget<TextField>(textFieldFinder);
  expect(textField.controller?.text, expectedValue);
}
