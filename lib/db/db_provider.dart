// SPDX-FileCopyrightText: Copyright 2023-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: MIT
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'dart:io';

import 'package:todo/model/todo_model.dart';
import 'package:todo/model/task_model.dart';
import 'package:sqflite_common/sqlite_api.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

class DBProvider {
  static Database? _database;

  DBProvider._();
  static final DBProvider db = DBProvider._();

  final List<Task> tasks = [
    Task(
      'Shopping',
      id: '1',
      color: Colors.purple.value,
      codePoint: Icons.shopping_cart.codePoint,
    ),
    Task(
      'Workout',
      id: '2',
      color: Colors.pink.value,
      codePoint: Icons.fitness_center.codePoint,
    ),
  ];

  final List<Todo> todos = [
    Todo(
      'Vegetables',
      id: '1',
      parent: '1',
    ),
    Todo(
      'Birthday gift',
      id: '2',
      parent: '1',
    ),
    Todo(
      'Chocolate cookies',
      id: '3',
      parent: '1',
      isCompleted: 1,
    ),
    Todo(
      '20 pushups',
      id: '4',
      parent: '2',
    ),
    Todo(
      'Tricep',
      id: '5',
      parent: '2',
    ),
    Todo(
      '15 burpees (3 sets)',
      id: '6',
      parent: '2',
    ),
  ];

  Future<Database> get database async {
    return _database ??= await initDB();
  }

  Future<String> get _dbPath async {
    String documentsDirectory = await _localPath;
    return p.join(documentsDirectory, "Todo.db");
  }

  Future<bool> dbExists() async {
    return File(await _dbPath).exists();
  }

  Future<Database> initDB() async {
    String path = await _dbPath;
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) async {
        await _ensureTestData(db);
      },
      onCreate: (Database db, int version) async {
        print("DBProvider:: onCreate()");
        await db.execute("""
          CREATE TABLE IF NOT EXISTS Task (
            id TEXT PRIMARY KEY,
            name TEXT,
            color INTEGER,
            code_point INTEGER
          )
        """);
        await db.execute("""
          CREATE TABLE IF NOT EXISTS Todo (
            id TEXT PRIMARY KEY,
            name TEXT,
            parent TEXT,
            completed INTEGER NOT NULL DEFAULT 0
          )
        """);
        await _ensureTestData(db);
      },
    );
  }

  Future<void> _ensureTestData(Database db) async {
    int taskCount =
        Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM Task')) ??
            0;
    if (taskCount == 0) {
      // Inserting test tasks else test tasks already exist
      await insertBulkTask(db, tasks);
    }

    int todoCount =
        Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM Todo')) ??
            0;
    if (todoCount == 0) {
      // Inserting test Todos else test Todos already exist
      await insertBulkTodo(db, todos);
    }
  }

  Future<void> insertBulkTask(Database db, List<Task> tasks) async {
    Batch batch = db.batch();
    for (var task in tasks) {
      batch.insert("Task", task.toJson());
    }
    await batch.commit(noResult: true);
  }

  Future<void> insertBulkTodo(Database db, List<Todo> todos) async {
    Batch batch = db.batch();
    for (var todo in todos) {
      batch.insert("Todo", todo.toJson());
    }
    await batch.commit(noResult: true);
  }

  Future<List<Task>> getAllTask() async {
    final db = await database;
    var result = await db.query('Task');
    return result.map((it) => Task.fromJson(it)).toList();
  }

  Future<List<Todo>> getAllTodo() async {
    final db = await database;
    var result = await db.query('Todo');
    return result.map((it) => Todo.fromJson(it)).toList();
  }

  Future<int> updateTodo(Todo todo) async {
    final db = await database;
    return db
        .update('Todo', todo.toJson(), where: 'id = ?', whereArgs: [todo.id]);
  }

  Future<int> removeTodo(Todo todo) async {
    final db = await database;
    return db.delete('Todo', where: 'id = ?', whereArgs: [todo.id]);
  }

  Future<int> insertTodo(Todo todo) async {
    final db = await database;
    return db.insert('Todo', todo.toJson());
  }

  Future<int> insertTask(Task task) async {
    final db = await database;
    return db.insert('Task', task.toJson());
  }

  Future<void> removeTask(Task task) async {
    final db = await database;
    return db.transaction<void>((txn) async {
      await txn.delete('Todo', where: 'parent = ?', whereArgs: [task.id]);
      await txn.delete('Task', where: 'id = ?', whereArgs: [task.id]);
    });
  }

  Future<int> updateTask(Task task) async {
    final db = await database;
    return db
        .update('Task', task.toJson(), where: 'id = ?', whereArgs: [task.id]);
  }

  Future<String> get _localPath async {
    final directory = await getApplicationSupportDirectory();
    return directory.path;
  }

  Future<void> closeDB() async {
    await _database?.close();
  }
}
