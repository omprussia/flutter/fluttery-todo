// import 'dart:convert';
// import 'dart:io';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

// import 'package:objectdb/objectdb.dart';

import 'package:todo/model/todo_model.dart';
import 'package:todo/model/task_model.dart';
import 'package:todo/db/db_provider.dart';

class TodoListModel extends Model {
  // ObjectDB db;
  var _db = DBProvider.db;
  List<Todo> get todos => _todos.toList();
  List<Task> get tasks => _tasks.toList();
  int getTaskCompletionPercent(Task task) =>
      _taskCompletionPercentage[task.id] ?? 0;
  int getTotalTodosFrom(Task task) =>
      todos.where((it) => it.parent == task.id).length;
  bool get isLoading => _isLoading;

  bool _isLoading = false;
  List<Task> _tasks = [];
  List<Todo> _todos = [];
  Map<String, int> _taskCompletionPercentage = Map();

  static TodoListModel of(BuildContext context) =>
      ScopedModel.of<TodoListModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
    if (!_isLoading && _tasks.isEmpty && _todos.isEmpty) {
      _isLoading = true;
      loadTodos();
    }
  }

  void loadTodos() async {
    _tasks = await _db.getAllTask();
    _todos = await _db.getAllTodo();
    _tasks.forEach((it) => _calcTaskCompletionPercent(it.id));
    _isLoading = false;
    await Future.delayed(Duration(milliseconds: 300));
    notifyListeners();
  }

  @override
  void removeListener(VoidCallback listener) {
    super.removeListener(listener);
    print("remove listner called");
    // DBProvider.db.closeDB();
  }

  void addTask(Task task) {
    _tasks.add(task);
    _calcTaskCompletionPercent(task.id);
    _db.insertTask(task).then((_) {
      notifyListeners();
    });
  }

  void removeTask(Task task) {
    _db.removeTask(task).then((_) {
      _tasks.removeWhere((it) => it.id == task.id);
      _todos.removeWhere((it) => it.parent == task.id);
      notifyListeners();
    });
  }

  void updateTask(Task task) {
    var oldTask = _tasks.firstWhere((it) => it.id == task.id);
    var replaceIndex = _tasks.indexOf(oldTask);
    if (replaceIndex != -1) {
      _tasks.replaceRange(replaceIndex, replaceIndex + 1, [task]);
      _db.updateTask(task).then((_) {
        notifyListeners();
      });
    }
  }

  void removeTodo(Todo todo) {
    _todos.removeWhere((it) => it.id == todo.id);
    _syncJob(todo);
    _db.removeTodo(todo).then((_) {
      notifyListeners();
    });
  }

  void addTodo(Todo todo) {
    _todos.add(todo);
    _syncJob(todo);
    _db.insertTodo(todo).then((_) {
      notifyListeners();
    });
  }

  void updateTodo(Todo todo) {
    var oldTodo = _todos.firstWhere((it) => it.id == todo.id);
    var replaceIndex = _todos.indexOf(oldTodo);
    if (replaceIndex != -1) {
      _todos.replaceRange(replaceIndex, replaceIndex + 1, [todo]);

      _syncJob(todo);
      _db.updateTodo(todo).then((_) {
        notifyListeners();
      });
    }
  }

  _syncJob(Todo todo) {
    _calcTaskCompletionPercent(todo.parent);
    // _syncTodoToDB();
  }

  void _calcTaskCompletionPercent(String taskId) {
    var relatedTodos = this.todos.where((it) => it.parent == taskId);
    var totalTodos = relatedTodos.length;

    if (totalTodos == 0) {
      _taskCompletionPercentage[taskId] = 0;
    } else {
      var totalCompletedTodos =
          relatedTodos.where((it) => it.isCompleted == 1).length;
      _taskCompletionPercentage[taskId] =
          (totalCompletedTodos / totalTodos * 100).toInt();
    }
    // return todos.fold(0, (total, todo) => todo.isCompleted ? total + scoreOfTask : total);
  }

  // Future<int> _syncTodoToDB() async {
  //   return await db.update({'user': 'guest'}, {'todos': _todos});
  // }
}
