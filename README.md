# Todo - Simple & Beautiful

<h4 align="center">A minimal Todo mobile app made using Flutter.</h4>

<p float="left">
  <img src="screenshots/screen01.png" width="200" />
  <img src="screenshots/screen02.png" width="200" /> 
  <img src="screenshots/screen03.png" width="200" />
  <img src="screenshots/screen04.png" width="200" />
</p>

## Features

* Easily add and remove tasks
* Organize tasks under categories. editing.
* Personalize task category using color and icon.

## Build_runner must be run before the application can be launched

```
dart run build_runner build
```
